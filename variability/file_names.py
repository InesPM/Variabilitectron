#!/usr/bin/env python3
# coding=utf-8


################################################################################
#                                                                              #
# Variabilitectron - Searching variability into XMM-Newton data                #
#                                                                              #
# Declaration of file names                                                    #
#                                                                              #
# Inés Pastor Marazuela (2018) - ines.pastor.marazuela@gmail.com               #
#                                                                              #
################################################################################
"""
Declaration of the file names handled both by the detector and the renderer
"""



LOG = "log.txt"

VARIABILITY = "variability_file.csv"
EVTS_PX_TW = "events_count_per_px_per_tw.csv"
VARIABLE_AREAS = "detected_variable_areas_file.csv"
TIME_WINDOWS = "time_windows_file.csv"
VARIABLE_SOURCES = "detected_variable_sources.csv"

OUTPUT_IMAGE = "variability.pdf"
OUTPUT_ANIMATION = "animation.gif"
OUTPUT_IMAGE_SRCS = "sources.pdf"
