#!/usr/bin/env python3
# coding=utf-8

'''
Getting the probability of constancy from analysed sources
'''
'''
Input arguments: <catalogue> <refiltered [yes/no]> <DL> <TW> <GTR>
'''

# Imports 
from math import *
import sys
#import os
import re
import glob
import subprocess


# Useful variables
catalogue  = sys.argv[1]
refiltered = sys.argv[2]
DL         = int(sys.argv[3])
TW         = int(sys.argv[4])
GTR        = float(sys.argv[5])
parameters = '{0}_{1}_{2}'.format(DL,TW,GTR)
pathr      = '/home/pastor/variability/{0}_refiltered/'.format(catalogue)
path0      = '/home/pastor/variability/{0}/'.format(catalogue)

names_noTW = []
names_TW   = []

# Opening files
#log        = open(path + 'log_sources.txt','r')
log        = open('/home/pastor/variability/{0}_{1}_log_sources_new.txt'.format(catalogue,parameters),'r')
if refiltered == 'yes' :
	proba      = open(pathr + 'proba_{0}_new.txt'.format(parameters),'w')
else :
	proba      = open(path0 + 'proba_{0}_new.txt'.format(parameters),'w')

# Main programme
i = 0

for line in log :
	names      = []
	names_noTW = []
	names_TW   = []
	cols = line.split()
	observation = cols[0]
	if refiltered == 'yes' :
		if cols[2] != '0' and cols[1] == '0' :
			names_noTW  = glob.glob(pathr + '{0}/J*_lccorr.lc'.format(observation))
			names_TW    = glob.glob(pathr + '{0}/J*_lccorr_{1}.lc'.format(observation,TW))
		elif cols[2] != '0' and cols[1] != '0' and int(cols[2]) > int(cols[1]) :
			names_noTW  = glob.glob(pathr + '{0}/J*_lccorr.lc'.format(observation))
			names_TW    = glob.glob(pathr + '{0}/J*_lccorr_{1}.lc'.format(observation,TW))
			names_noTW.append(glob.glob(path0 + '{0}/J*_lccorr.lc'.format(observation)))
			names_TW.append(glob.glob(path0 + '{0}/J*_lccorr_{1}.lc'.format(observation,TW)))
		elif cols[2] != '0' and cols[1] != '0' and int(cols[2]) <= int(cols[1]) :
			names_noTW = glob.glob(pathr + '{0}/J*_lccorr.lc'.format(observation))
			names_TW   = glob.glob(pathr + '{0}/J*_lccorr_{1}.lc'.format(observation,TW))
			names_noTW.append(glob.glob(path0 + '{0}/J*_lccorr.lc'.format(observation)))
			names_TW.append(glob.glob(path0 + '{0}/J*_lccorr_{1}.lc'.format(observation,TW)))

		if cols[2] != 0 :
			bad = cols.count('bad')
			if bad != 0 and bad <= int(cols[2]) and int(cols[1]) <= int(cols[2]):
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'bad',bad))
				print('{0}\t{1}\t{2}\t{3}'.format(observation,'bad',bad, int(cols[2])))		
			if 'fluct' in cols :
				fluct = cols.count('fluct')
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'fluct',fluct))
				print('{0}\t{1}\t{2}'.format(observation,'fluct',fluct))
			if 'ext' in cols :
				fluct = cols.count('ext')
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'ext',fluct))
				print('{0}\t{1}\t{2}'.format(observation,'ext',fluct))

	else :
		names_noTW = glob.glob(path0 + '{0}/J*_lccorr.lc'.format(observation))
		names_TW   = glob.glob(path0 + '{0}/J*_lccorr_{1}.lc'.format(observation,TW))
		
		if cols[1] != 0:
			if 'bad' in cols :
				bad = cols.count('bad')
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'bad',bad))
				print('{0}\t{1}\t{2}'.format(observation,'bad',bad))		
			if 'fluct' in cols :
				fluct = cols.count('fluct')
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'fluct',fluct))
				print('{0}\t{1}\t{2}'.format(observation,'fluct',fluct))
			if 'ext' in cols :
				fluct = cols.count('ext')
				proba.write('{0}\t{1}\t{2}\n'.format(observation,'ext',fluct))
				print('{0}\t{1}\t{2}'.format(observation,'ext',fluct))
			
	# Real sources
	if names_noTW != [] :
		names = (names_noTW)
		
	if names_TW != [] :
		names = (names_TW)

	if names != [] and names != [[]] :
		for source in names :
			if source != [] :
				#print(source,type(source))
				J       = re.search(r'J', source)
				src     = source[J.start():J.start() + 14]
				lcstats = str(subprocess.check_output(['lcstats', 'cfile1={0}'.format(source), 'window="-"', 'dtnb=0.0734'.format(TW), 'nbint=1000000', 'tchat=2']))
				c_s = re.search(r'Chi-Square Prob of constancy. ', lcstats)
				k_s = re.search(r'Kolm.-Smir. Prob of constancy ', lcstats)
				
				if c_s is not None and k_s is not None :
					chi_square = float(lcstats[c_s.end():c_s.end()+11])
					if chi_square == 0.0 :
						chi_square = 1e-38
					kolm_smir  = float(lcstats[k_s.end():k_s.end()+11])
					if kolm_smir == 0.0 :
						kolm_smir = 1e-38

					proba.write('{0}\t{1}\t{2:<10}\t{3:<10}\n'.format(observation,src,chi_square,kolm_smir))
					print('{0}\t{1}\t{2:<10}\t{3:<10}'.format(observation,src,chi_square,kolm_smir))



# Closing files
log.close()
proba.close()
