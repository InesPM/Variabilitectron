#!/bin/bash

################################################################################
#                                                                              #
# Variabilitectron - Searching variability into XMM-Newton data                #
#                                                                              #
# Automatic lightcurve generation of the detected variablerces                 #
#                                                                              #
# Inés Pastor Marazuela (2018) - ines.pastor.marazuela@gmail.com               #
#                                                                              #
################################################################################

# bash script generating lightcurves and computing the chi-square and
# Kolmogorov-Smirnov probability of constancy for sources detected by
# Variabilitectron as being variable

# bash /scratch/ines/progs/lightcurve.sh <path_obs> <path_scripts> <id> <DL> <TW> <output_log>

# Style functions
################################################################################

title1(){
  message=$1
	i=0; x='===='
	while [[ i -lt ${#message} ]]; do x='='$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x\n"
}

title2(){
  message=$1
	i=0; x=----
	while [[ i -lt ${#message} ]]; do x=-$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x"
}

title3(){
  message=$1
  echo -e "\n # $message"
}


################################################################################
#                                                                              #
# Preliminaries                                                                #
#                                                                              #
################################################################################

#read arguments

# Printing help
if [[ $1 == "-h" ]] || [[ $1 == "--help" ]] ; then
	echo -e "\
	Parameters to use :\n\
	@path       : full path to the observation\n\
  @scripts    : full path to the scripts folder\n\
	@id         : id number of the detected source within the observation\n\
	@DL         : Detection level used for the variable sources detection\n\
	@TW         : Time window used for the variable sources detection\n\
	@output_log : full path to the document storing the information of the detection\n\
	"
	exit

else

path="$1"
scripts="$2"
id="$3"
DL="$4"
TW="$5"
output_log="$6"

observation=${path: -10}

title1 "Lightcurve Obs. $observation Src. $id"

counts=20	# Counts considered to group the spectra

# Selecting the files and paths
clean_file=$path/PN_clean.fits
gti_file=$path/PN_gti.fits
att_file=$(ls $path/*_AttHk.ds)
img_file=$path/PN_image.fits
nosrc_file=$path/PN_sourceless.fits
path_out=$path/lcurve_${TW}
if [ ! -d $path_out ]; then mkdir $path_out; fi
cd $path_out

# Setting SAS tools
export SAS_ODF=$path
export SAS_CCF=$path/ccf.cif
export SAS_CCFPATH=/home/ines/Downloads/xmmsas_20180620_1732/ccf
export HEADAS=/home/ines/Downloads/heasoft-6.25/x86_64-pc-linux-gnu-libc2.27

. $HEADAS/headas-init.sh
. /home/ines/Downloads/xmmsas_20180620_1732/setsas.sh

################################################################################
#                                                                              #
# Source selection                                                             #
#                                                                              #
################################################################################

title2 "Preliminaries"

if [ ! -f $path/${DL}_${TW}_0.9/sources.pdf ]; then python3 $scripts/renderer.py $path/${DL}_${TW}_0.9 $clean_file; fi
if [ ! -f $img_file ]; then
  evselect table=$clean_file imagebinning=binSize imageset=$img_file withimageset=yes xcolumn=X ycolumn=Y ximagebinsize=80 yimagebinsize=80 -V 0
fi

###
# Reading data from the detected_variable_sources file
###

data=$(cat $path/${DL}_${TW}_0.9/detected_variable_sources.csv | grep "^${id};")

###
# Defining source position
###

IFS=';' read -r -a array <<< "$data"
n=${array[0]}
ccd=${array[1]}
rawx=${array[2]}
rawy=${array[3]}
srcR=$((${array[4]} * 64))				# Pixel units

echo "ccd=$ccd, rawx=$rawx, rawy=$rawy, r=$srcR"

srcXY=$(ecoordconv imageset=$path/PNimage.fits coordtype=raw x=$rawx y=$rawy ccdno=$ccd | tee /dev/tty|grep 'X: Y:'|sed 's/X: Y: //'|sed 's/ /,/g'|sed 's/,//')

# Correcting source and background position
srcexp=$(eregionanalyse imageset=$path/PNimage.fits srcexp="(X,Y) in CIRCLE($srcXY,$srcR)" backval=0.1|tee /dev/tty|grep 'SASCIRCLE'|sed 's/SASCIRCLE: //g')
srcR=$(echo $srcexp | sed "s/(X,Y) in CIRCLE([0-9]*.[0-9]*,[0-9]*.[0-9]*,//" | sed "s/)//")
# arcsec
srcRas=$(bc <<< "scale=2; $srcR * 0.05")

###
# Source name and coordinates
###
srccoord=$(ecoordconv imageset=$path/PNimage.fits coordtype=raw x=$rawx y=$rawy ccdno=$ccd | tee /dev/tty|grep ' RA: DEC: ' | sed 's/ RA: DEC: //g')

# Right ascension
# Decimal degrees
RAd=$(echo $srccoord | sed 's/ [0-9]*.[0-9]*//g')
# h
RA=$(bc <<< "scale=5; $RAd / 15")
if [[ $RA == .* ]]; then RA=0$RA; fi
h=$(echo $RA | sed "s/.[0-9]*$//g")
m=$(bc <<< "scale=5; ($RA - $h) * 60" | sed "s/.[0-9]*$//g")
s=$(bc <<< "scale=2; (($RA - $h) * 60 - $m) * 60" | sed "s/.[0-9]*$//")
if [ ${#h} == 1 ]; then h=0$h ; fi
if [ ${#m} == 1 ]; then m=0$m ; fi
if [ ${#s} == 1 ]; then s=0$s ; elif [ ${#as} == 0 ]; then as=00; fi

# Declination
DEC=$(echo $srccoord | sed 's/[0-9]*.[0-9]* //g')
if [[ $DEC == -* ]]; then DEC=${DEC#"-"}; link=-; else link=_; fi
if [[ $DEC == .* ]]; then DEC=0$DEC; fi
dg=$(echo $DEC | sed "s/.[0-9]*$//g")
am=$(bc <<< "scale=5; ($DEC - $dg) * 60" | sed "s/.[0-9]*$//g")
as=$(bc <<< "scale=2; (($DEC - $dg) * 60 - $am) * 60" | sed "s/.[0-9]*$//")
if [ ${#dg} == 1 ]; then dg=0$dg ; fi
if [ ${#am} == 1 ]; then am=0$am ; fi
if [ ${#as} == 1 ]; then as=0$as ; elif [ ${#as} == 0 ]; then as=00; fi

# Source name
src=J$h$m$s$link$dg$am$as
echo -e "\n\t$src\n"

###
# Background extraction region
###

# Contaminating sources
if [ ! -f $path/list_sources.fits ] && [ ! -f $path/list_regions.fits ] ; then
  title2 "edetect_chain"
  edetect_chain imagesets="$img_file" eventsets="$clean_file" pimin="500" pimax="12000" attitudeset="$att_file" eml_list="$path/list_sources.fits" -V 0
  python3 $scripts/regions.py $path/list_sources.fits
  # Creating regions file
  region eventset="$path/PN_clean.fits" srclisttab="$path/list_sources.fits" regionset="$path/list_regions.fits" operationstyle="batch" srcidcol="ML_ID_SRC" -V 0
  python3 $scripts/regions.py $path/list_regions.fits $path/list_regions.txt
fi

evselect table=$clean_file withfilteredset=Y filteredset=$nosrc_file.fits destruct=Y keepfilteroutput=T expression="$(cat $path/list_regions.txt)" -V 0

bgdXY=$(ebkgreg withsrclist=no withcoords=yes imageset=$img_file x=$RAd y=$DEC r=$srcRas coordtype=EQPOS | grep 'X,Y Sky Coord.' | sed "s/  Extraction radius (X,Y Sky Coord.) : [0-9]*.[0-9]*//g" | sed "s/X,Y Sky Coord.//" | sed "s/://g" | sed "s/ //g")
bgdexp="(X,Y) in CIRCLE($bgdXY,$srcR)"

#echo -e "\n\nsrcXY\t$srcXY\nsrcexp\t$srcexp\nbgdexp\t$bgdexp"

echo -e "\nExtracting data from source $src with the following coordinates: \n  Source     : $srcexp\n  Background : $bgdexp)"


################################################################################
#                                                                              #
# Lightcurve generation                                                        #
#                                                                              #
################################################################################

title2 "Extracting lightcurve"
if [ ! -f $path/PN_gti.wi ]; then gti2xronwin -i $path/PN_gti.fits -o $path/PN_gti.wi; fi

title3 "evselect 0.0734 s"
evselect table=$clean_file energycolumn=PI expression="$srcexp" withrateset=yes rateset=$path_out/${src}_lc_0.0734_src.lc timebinsize=0.0734 maketimecolumn=yes makeratecolumn=yes -V 0
evselect table=$nosrc_file energycolumn=PI expression="$bgdexp" withrateset=yes rateset=$path_out/${src}_lc_0.0734_bgd.lc timebinsize=0.0734 maketimecolumn=yes makeratecolumn=yes -V 0

title3 "evselect 100 s"
evselect table=$clean_file energycolumn=PI expression="$srcexp" withrateset=yes rateset=$path_out/${src}_lc_${TW}_src.lc timebinsize=$TW maketimecolumn=yes makeratecolumn=yes -V 0
evselect table=$nosrc_file energycolumn=PI expression="$bgdexp" withrateset=yes rateset=$path_out/${src}_lc_${TW}_bgd.lc timebinsize=$TW maketimecolumn=yes makeratecolumn=yes -V 0

title3 "epiclccorr"
epiclccorr srctslist=$path_out/${src}_lc_${TW}_src.lc eventlist=$clean_file outset=$path_out/${src}_lccorr_${TW}.lc bkgtslist=$path_out/${src}_lc_${TW}_bgd.lc withbkgset=yes applyabsolutecorrections=yes -V 0

title3 "lcstats"
lcstats cfile1="$path_out/${src}_lccorr_${TW}.lc" window=$path/PN_gti.wi dtnb=$TW nbint=1000000 tchat=2 logname="$path_out/${src}_xronos.log"
P=$(lcstats cfile1="$path_out/${src}_lccorr_${TW}.lc" window=$path/PN_gti.wi dtnb=$TW nbint=1000000 tchat=2 logname="$path_out/${src}_xronos.log" | grep "Prob of constancy")
P_chisq=$(echo $P | sed "s/Chi-Square Prob of constancy. //" | sed "s/ (0 means.*//")
P_KS=$(echo $P | sed "s/.*Kolm.-Smir. Prob of constancy //" |  sed "s/ (0 means.*//")

echo -e "Probabilities of constancy : \n\tP_chisq = $P_chisq\n\tP_KS    = $P_KS"

title3 "lcurve"
lcurve nser=2 cfile1="$path_out/${src}_lc_${TW}_src.lc" cfile2="$path_out/${src}_lc_${TW}_bgd.lc" window=$path/PN_gti.wi dtnb=$TW nbint=1000000 tchat=2 outfile="$path_out/${src}_lc_${TW}.flc" plotdev="/XW" plot=yes plotdnum=2 plotfile=$scripts/lcurve.pco
if [ -f "$(ls $path_out/pgplot.ps)" ]; then
	ps2pdf $path_out/pgplot.ps $path_out/${src}_lc_${TW}_xronos.pdf
fi

echo -e "python3 $scripts/lcurve.py -src $path_out/${src}_lc_${TW}_src.lc -bgd $path_out/${src}_lc_${TW}_bgd.lc -gti $path/PN_gti.fits -dtnb $TW -outdir $path_out -name $src -text \$P_{chi^2} = $P_chisq$;\$P_{KS} = $P_KS$ -obs $observation -id $id"

python3 $scripts/lcurve.py -src $path_out/${src}_lc_${TW}_src.lc -bgd $path_out/${src}_lc_${TW}_bgd.lc -gti $path/PN_gti.fits -dtnb $TW -outdir $path_out -name $src -text "\$P_{\chi^2} = $P_chisq$;\$P_{KS} = $P_KS$" -obs $observation -id $id

###
# Writing output to files, ending script
###

echo -e > $path_out/${src}_region.txt "Source     = $srcexp\nBackground = $bgdexp"
echo -e >> $output_log "$observation $src $DL $TW $P_chisq $P_KS"

echo -e "\nObservation $observation ended"
fi
