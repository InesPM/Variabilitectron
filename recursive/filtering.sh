#!/bin/bash

################################################################################
#                                                                              #
# Variabilitectron - Searching variability into XMM-Newton data                #
#                                                                              #
# Events file generation and variability computation                           #
#                                                                              #
# Inés Pastor Marazuela (2018) - ines.pastor.marazuela@gmail.com               #
#                                                                              #
################################################################################

# bash script generating the filtered events file and the GTI file and computing the variability.

# bash variabilitectron.sh <FOLDER> <DL> <TW> <GTR>

################################################################################
#                                                                              #
# Defining the functions                                                       #
#                                                                              #
################################################################################

# Style functions
################################################################################

Title(){
  message=$1
	i=0; x='===='
	while [[ i -lt ${#message} ]]; do x='='$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x\n"
}

title(){
  message=$1
	i=0; x=----
	while [[ i -lt ${#message} ]]; do x=-$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x\n"
}

# Data analysis functions
################################################################################

preliminaries(){

	title "Preliminaries"
	path=$1/$2
	obs=$2

	# Setting up SAS
	cd $path
	export SAS_ODF=$path
	export SAS_CCF=$path/ccf.cif
  export SAS_CCFPATH=/home/ines/Downloads/xmmsas_20180620_1732/ccf
	export HEADAS=/home/ines/Downloads/heasoft-6.25/x86_64-pc-linux-gnu-libc2.27

	. $HEADAS/headas-init.sh
	. /home/ines/Downloads/xmmsas_20180620_1732/setsas.sh

	if [ ! -f $path/ccf.cif ]; then cifbuild; else echo "CIF already built"; fi
	if [ ! -f $(ls $path/*SUM.SAS) ]; then odfingest; else echo "ODF already ingested"; fi
	if [ ! -f $(ls $path/*ImagingEvts*) ]; then epproc; else echo "EP already processed"; fi
  # cifbuild
  # odfingest
  # epproc

	cp $path/*ImagingEvts* $path/PN.fits

}

filtering(){

	title "Cleaning events file"
	path=$1/$2
	obs=$2

	# File names
	org_file=$path/PN_org.fits
	events_file=$path/PN_clean.fits
	gti_file=$path/PN_gti.fits
	rate_file=$path/PN_rate.fits

	if [ ! -f $org_file ]; then cp $(ls $path/*ImagingEvts*) $org_file; fi

	# Creating GTI
	evselect table=$org_file withrateset=Y rateset=$rate_file maketimecolumn=Y timebinsize=100 makeratecolumn=Y expression='#XMMEA_EP && (PI in [10000:12000]) && (PATTERN==0)' -V 0

	rate=0.4	# use bkgoptrate instead
	echo "Creating Good Time Intervals with threshold RATE=$rate"

	tabgtigen table=$rate_file expression="RATE<=$rate" gtiset=$gti_file -V 0

	# Cleaning events file
	evselect table=$org_file withfilteredset=Y filteredset=$events_file destruct=Y keepfilteroutput=T expression="#XMMEA_EP && gti($gti_file,TIME) && (PATTERN<=4) && (PI in [500:12000])" -V 0

	echo > $path/PN_processing.log "Rate: $rate"
}

# Main programme

FOLDER=$1
obs=$2

echo -e "\nFOLDER=$1\nobs=$2\n"

# Filtering observations
preliminaries $FOLDER $obs
filtering $FOLDER $obs
