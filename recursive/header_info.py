#!/usr/bin/env python3
# coding=utf-8

'''
Getting the rate from the events file header
'''

# Built-il imports
import sys
import os
from astropy.io import fits


# Extracting the data
hdulist1 = fits.open(sys.argv[1])
header1  = hdulist1['RATE'].header
RATE     = header1['FLCUTTHR']

hdulist2 = fits.open(sys.argv[2])
header2  = hdulist2[0].header
DATAMODE = header2['DATAMODE']
SUBMODE  = header2['SUBMODE']
print(RATE, DATAMODE, SUBMODE)
sys.exit(0)
hdulist1.close()
hdulist2.close()