#!/usr/bin/env python3
# coding=utf-8

'''
Histogram of the probabilities
'''

from math import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *


# Useful variables
DL         = 10
TW         = 100
GTR        = 0.9
parameters = '{0}_{1}_{2}'.format(DL,TW,GTR)
path       = '/home/ines/stage/data/'

# Opening files
proba      = open(path + 'proba_{0}.txt'.format(parameters),'r')

# Extracting data from files
chi_square = []
kolm_smir  = []
c_s = 0; k_s = 0; n_v = 0; c_k = 0

for lines in proba :
	cols = lines.split()
	chi_square.append(cols[2])
	kolm_smir.append(cols[3])
	#if float(cols[2]) < 1e-4 :
		#c_s.append(cols[2])
	#if float(cols[3]) < 1e-4 :
		#k_s.append(cols[3])
	if float(cols[2]) > 1e-4 and float(cols[3]) > 1e-4 :
		n_v += 1	# Non variable
	elif float(cols[2]) < 1e-4 and float(cols[3]) > 1e-4 :
		c_s += 1	# chi-square variable
	elif float(cols[2]) > 1e-4 and float(cols[3]) < 1e-4 :
		k_s += 1	# kolmogorov-smirnov variable
	else :
		c_k += 1	# both variable
# Closing files
proba.close()

chi_square = np.array(chi_square).astype(np.float)
kolm_smir = np.array(kolm_smir).astype(np.float)

#c_s = np.array(c_s).astype(np.float)
#k_s = np.array(k_s).astype(np.float)

#print('c-s and k-s variable: {0}/{4}\nc-s variable: {1}/{4}\nk-s variable: {2}/{4}\nnon variable: {3}/{4}'.format(c_k,c_s,k_s,n_v,len(chi_square)))

values = [c_k,k_s,c_s,n_v]
labels = ['$\chi^2$ and K-S','K-S only', '$\chi^2$ only', 'non variable']
total  = len(chi_square)

###
# Plots
###
bins = np.logspace(-42, 1, 30)

#plt.hist(gaussian_numbers, bins)
#plt.plot(np.linspace(0,10,100),np.linspace(0,20,100))

# Histogram
plt.figure(1)
plt.hist(chi_square, bins, alpha=0.3, lw=3, label='P($\chi^2$)',color='g')
plt.hist(kolm_smir, bins, alpha=0.3, lw=3, label='P(KS)',color='c')
plt.axvline(x=1e-4, ymin=0.0, ymax=1e2, label='P = $10^{-4}$')
#plt.hist(c_s, bins, alpha=0.5)
#plt.hist(k_s, bins, alpha=0.5)
plt.title("Probability of constancy histogram for {0} sourceswith TW={1} DL={2} GTR={3}".format(total,TW,DL,GTR))
plt.xlabel("Value")
plt.ylabel("Frequency")
plt.xscale('log')
#plt.yscale('log')
plt.legend()
plt.xlim(1e-42,1)
plt.savefig(path + 'histogram_{0}.pdf'.format(parameters), pad_inches=0, bbox_inches='tight', dpi=500)

# Scatter
plt.figure(2)
plt.plot(chi_square, kolm_smir, 'g+')
plt.axvline(x=1e-4, ymin=0.0, ymax=1e2, label='P = $10^{-4}$')
plt.axhline(y=1e-4, xmin=0.0, xmax=1e2, label='P = $10^{-4}$')
plt.title("Probability of constancy for {0} sources with TW={1} DL={2} GTR={3}".format(total,TW,DL,GTR))
plt.xlabel("P($\chi^2$)")
plt.ylabel("P(KS)")
plt.text(1.5e-4, 1.5e-4, n_v)
plt.text(1.5e-42,1.5e-43,c_k)
plt.text(1.5e-4,1.5e-43,k_s)
plt.text(1.5e-42,1.5e-4,c_s)
plt.xscale('log')
plt.yscale('log')
plt.xlim(1e-43,1)
plt.ylim(1e-43,1)
#plt.legend()
plt.xlim(1e-42,1)
plt.savefig(path + 'c-s_k-s_{0}.pdf'.format(parameters), pad_inches=0, bbox_inches='tight', dpi=500)

# Pie
plt.figure(3)
the_grid = GridSpec(2, 2)
plt.subplot(the_grid[0, 0], aspect=1)
plt.pie(values, labels=labels, autopct='%1.1f%%', shadow=True)
plt.title("{0} sources with TW={1} DL={2} GTR={3}".format(total,TW,DL,GTR))
plt.savefig(path + 'pie_{0}.pdf'.format(parameters), pad_inches=0, bbox_inches='tight', dpi=500)

# Bar chart
plt.figure(4)
position = [1,2,3,4]
barlist=plt.bar(position, values, align='center')
plt.xticks(position, labels)
plt.title("{0} sources with TW={1} DL={2} GTR={3}".format(total,TW,DL,GTR))
for i in range(len(values)) :
	plt.text(position[i],values[i],'{:.2f} $\%$'.format(values[i]/total*100),ha='center')
barlist[0].set_color('b')
barlist[1].set_color('c')
barlist[3].set_color('g')

plt.savefig(path + 'bar_{0}.pdf'.format(parameters), pad_inches=0, bbox_inches='tight', dpi=500)
