#!/bin/bash

################################################################################
#                                                                              #
# Variabilitectron - Searching variability into XMM-Newton data                #
#                                                                              #
# Events file generation and variability computation                           #
#                                                                              #
# Inés Pastor Marazuela (2018) - ines.pastor.marazuela@gmail.com               #
#                                                                              #
################################################################################

# bash script generating the filtered events file and the GTI file and computing the variability.

# bash variabilitectron.sh <FOLDER> <DL> <TW> <GTR>

################################################################################
#                                                                              #
# Defining the functions                                                       #
#                                                                              #
################################################################################

# Style functions
################################################################################

Title(){
  message=$1
	i=0; x='===='
	while [[ i -lt ${#message} ]]; do x='='$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x\n"
}

title(){
  message=$1
	i=0; x='----'
	while [[ i -lt ${#message} ]]; do x='-'$x; ((++i)); done
  echo -e "\n\t  $message \n\t$x\n"
}

waitForFinish()
{
  wait_file=$1
  # Waiting until file is created
  while [[ ! -f $wait_file ]]; do
    echo "waiting does not exist"
    sleep 10
  done
  # Waiting while waiting = True
  while [[ $(cat waiting) == True ]]; do
    echo "Waiting"
    sleep 100
  done
  # Erasing wait_file when waiting = False
  if [[ $(cat waiting) == False ]]; then rm $wait_file; fi
}


# Variability computation functions
################################################################################

filtering(){
  path_obs=$1
  path_scripts=$2
  obs=$3

  if [[ $count -lt $(( $nb_obs )) ]]; then
    echo "bash $path_scripts/filtering.sh $path_obs $obs" >> $path_obs/process_flt
  else
    echo -n "bash $path_scripts/filtering.sh $path_obs $obs ; " >> $path_obs/process_flt
    echo "echo 'False' > waiting" >> $FOLDER/process_flt
	fi
  # Count increades in the variabilitectron function
}

variabilitectron(){

	###
	# Defining files and directories
	###
	# Reading the arguments
	path_obs=$1
  path_scripts=$2
	obs=$3
	DL=$4
	TW=$5
	GTR=$6

	# File names
  path=$path_obs/$obs
	events_file=$path/PN_clean.fits
	gti_file=$path/PN_gti.fits

	###
	# Variability calculation
	###
	# writing detector and renderer commands to file that will be run in parallel

	if [[ $count -lt $(( $nb_obs )) ]]; then
		echo -n "python3 $path_scripts/detector.py $events_file $gti_file $path/${DL}_${TW}_${GTR} --box-size 5 --detection-level $DL --time-window $TW --good-time-ratio $GTR --max-threads-allowed 1 --output-log $path_obs/variable_sources ; " >> $path_obs/process_var
		echo "python3 $path_scripts/renderer.py $path/${DL}_${TW}_${GTR} $events_file" >> $path_obs/process_var

  else
    echo -n "python3 $path_scripts/detector.py $events_file $gti_file $path/${DL}_${TW}_${GTR} --box-size 5 --detection-level $DL --time-window $TW --good-time-ratio $GTR --max-threads-allowed 1 --output-log $path_obs/variable_sources ; " >> $path_obs/process_var
		echo -n "python3 $path_scripts/renderer.py $path/${DL}_${TW}_${GTR} $events_file ; " >> $path_obs/process_var
    echo "echo 'False' > $path_obs/waiting" >> $path_obs/process_var

	fi
	((++count))
}

lightcurves(){
  Title "Generating lightcurves"

  # Reading arguments
  path_obs=$1
  path_scripts=$2
  DL=$3
  TW=$4

  echo $path_obs

  # Output file
  rm $path_obs/sources_variability
  echo "Observation Source DL TW P_chisq P_KS" >> $path_obs/sources_variability

  # Reading file
  let i=0

  ls $path_obs/variable_sources
  cat $path_obs/variable_sources

  while IFS=$'\n' read -r line; do
  	line_data[i]="${line}"
  	((++i))
  done < $path_obs/variable_sources

  # Starting loop
  for data in "${line_data[@]:1:i-1}"; do

    IFS=' ' read -r -a array <<< "$data"
    obs=${array[0]}
    src=${array[1]}
    dl=${array[2]}
    tw=${array[3]}

    count=1
    #if [[ $dl == $DL ]] && [[ $tw == $TW ]]; then
      while [[ $count -le $src ]] ; do
        echo "bash $path_scripts/lightcurve.sh $path_obs/$obs $path_scripts $count $DL $TW $path_obs/sources_variability" >> $path_obs/process_lc
        ((++count))
      done
    #fi
  done
  echo "echo 'False' > $path_obs/waiting" >> $path_obs/process_lc
}

################################################################################
#                                                                              #
# Main programme                                                               #
#                                                                              #
################################################################################

# Defining the detection parameters
FOLDER=$1            	# FOLDER where obs are stored
DL=$2                   #10
TW=$3                   #100
GTR=$4                  #0.9

VARFILES=/home/ines/Documents/M2/stage/progs
CPUS=10

# Retrieving a list of observations
cd $FOLDER
files=(0*)
nb_obs=0
for f in ${files[@]}; do ((++nb_obs)); done
# nb_obs=1

i=0
count=1

# Removing existing log files to avoid overwriting them
logs=(detected_sources process_flt process_lc process_var sources_variability variable_sources)
for l in ${logs[@]} ; do if [ -f $l ]; then rm $l; fi; done

echo "Observation sources DL TW" >> variable_sources

###
# Launching the variability computation
###

time {

  Title "Writing commands to files"
  # Writing commands to files to run them in parallel
	for obs in "${files[@]:0:$nb_obs}"			#"${files[@]:0:10}"
	do
		title "Observation $obs"
		date

		# Filtering observations
    echo "echo 'True' > $FOLDER/waiting" >> process_flt
    filtering $FOLDER $VARFILES $obs

		# Variability computation
    echo "echo 'True' > $FOLDER/waiting" >> process_var
		variabilitectron $FOLDER $VARFILES $obs $DL $TW $GTR

		((++i))
	done

  # Filtering observations
  Title "Filtering observations"
  bash $VARFILES/parallel.sh $FOLDER/process_flt $CPUS
  waitForFinish $FOLDER/waiting

	# Running variabilitectron
  Title "Applying variabilitectron"
	bash $VARFILES/parallel.sh $FOLDER/process_var $CPUS
  waitForFinish $FOLDER/waiting

	# Generating lightcurves
  Title "Generating lightcurves"
  echo "echo 'True' > $FOLDER/waiting" >> process_lc
  lightcurves $FOLDER $VARFILES $DL $TW
  bash $VARFILES/parallel.sh $FOLDER/process_lc $CPUS
  waitForFinish $FOLDER/waiting

	echo -n "\nTotal execution time for $nb_obs : "
}
